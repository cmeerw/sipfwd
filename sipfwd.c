/*	-*- C -*-
 * Copyright (C) 2005-2022, Christof Meerwald
 * http://cmeerw.org
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 dated June, 1991.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <signal.h>
#include <limits.h>
#include <errno.h>
#include <libgen.h>

#include <fcntl.h>
#include <netdb.h>
#include <syslog.h>
#include <unistd.h>
#include <time.h>

#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/poll.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <sqlite3.h>
#include <osipparser2/osip_parser.h>

#if defined(HAVE_UDNS)
#include <udns.h>
#else
struct dns_srv
{
    int priority;
    int weight;
    int port;
    const char *name;
};
#endif


#define RETRY_EINTR_RC(rc, expr) \
    { \
        do \
        { \
            rc = (expr); \
        } while ((rc < 0) && (errno == EINTR) && !quit); \
    }

#define RETRY_EINTR(expr) \
    while (((expr) < 0) && (errno == EINTR) && !quit) \
    { }


static sqlite3 *db = NULL;
static sqlite3_stmt *stmt_domain = NULL;
static sqlite3_stmt *stmt_user = NULL;

static int verbose = 0;
static int local_port = 5060;
static char local_port_str[] = "5060";
static volatile sig_atomic_t quit = 0;
static int pidfd = -1;

static int nr_sock = 0;
static int sock[16];
static int sock_ipv4 = -1;
static int mapped_ipv4 = 0;
static char addrstr_ipv4[256] = "";
static int sock_ipv6 = -1;
static char addrstr_ipv6[256] = "";


/**
 * Set quit indicator.
 */
static void signal_quit(int signo)
{
    quit = 1;
}


/**
 * Daemonize the process.
 *
 * @return pipe-fd that should be closed once initialization is complete
 */
int daemonize(void)
{
    int pipefds[2];
    int fd;
    int rc;
    char buf;

    if (pipe(pipefds) < 0)
    {
        syslog(LOG_ALERT, "pipe: %m");
        return -1;
    }

    switch (fork())
    {
    case 0:
        break;

    case -1:
        // error
        syslog(LOG_ALERT, "fork: %m");
        RETRY_EINTR(close(pipefds[0]));
        RETRY_EINTR(close(pipefds[1]));
        return -1;

    default:
        // parent
        RETRY_EINTR(close(pipefds[1]));

        RETRY_EINTR_RC(rc, read(pipefds[0], &buf, sizeof(buf)));
        if ((rc == sizeof(buf)) && (buf == '\0'))
        {
            RETRY_EINTR(close(pipefds[0]));
            closelog();
            _exit(0);
        }
        else if (rc < 0)
        {
            syslog(LOG_ALERT, "read: %m");
        }
        else
        {
            syslog(LOG_ALERT, "daemon initialisation failed");
        }

        RETRY_EINTR(close(pipefds[0]));
        closelog();
        exit(1);
    }

    RETRY_EINTR(close(pipefds[0]));

    if (setsid() < 0)
    {
        syslog(LOG_ERR, "setsid: %m");
    }

    switch (fork())
    {
    case 0:
        break;

    case -1:
        // error
        syslog(LOG_ALERT, "fork: %m");
        RETRY_EINTR(close(pipefds[1]));
        return -1;

    default:
        // parent
        _exit(0);
    }


    RETRY_EINTR_RC(fd, open("/dev/null", O_RDWR));
    if (fd < 0)
    {
        syslog(LOG_ALERT, "open /dev/null: %m");
        RETRY_EINTR(close(pipefds[1]));
        return -1;
    }

    RETRY_EINTR_RC(rc, dup2(fd, STDIN_FILENO));
    if (rc < 0)
    {
        syslog(LOG_ALERT, "dup2 STDIN: %m");
        RETRY_EINTR(close(pipefds[1]));
        return -1;
    }

    RETRY_EINTR_RC(rc, dup2(fd, STDOUT_FILENO));
    if (rc < 0)
    {
        syslog(LOG_ALERT, "dup2 STDOUT: %m");
        RETRY_EINTR(close(pipefds[1]));
        return -1;
    }

    RETRY_EINTR_RC(rc, dup2(fd, STDERR_FILENO));
    if (rc < 0)
    {
        syslog(LOG_ALERT, "dup2 STDERR: %m");
        RETRY_EINTR(close(pipefds[1]));
        return -1;
    }

    RETRY_EINTR(close(fd));

    return pipefds[1];
}

/**
 * Daemon initialization including pid-file handling.
 */
int daemon_init(const char *pidname)
{
    struct flock fl;
    char buf[32];
    int rc, l;

    if (chdir("/") < 0)
    {
        syslog(LOG_ERR, "chdir: %m");
    }

    umask(0);

    RETRY_EINTR_RC(pidfd, open(pidname, O_RDWR | O_CREAT, 0644));
    if (pidfd < 0)
    {
        syslog(LOG_ALERT, "open pid file: %m");
        return -1;
    }

    fl.l_type = F_WRLCK;
    fl.l_whence = SEEK_SET;
    fl.l_start = 0;
    fl.l_len = 0;

    if (fcntl(pidfd, F_SETLK, &fl) < 0)
    {
        if ((errno == EACCES) || (errno == EAGAIN))
        {
            syslog(LOG_ERR, "pid file already locked");
        }
        else
        {
            syslog(LOG_ALERT, "fcntl SETLK: %m");
        }

        RETRY_EINTR(close(pidfd));
        pidfd = -1;
        return -1;
    }

    RETRY_EINTR_RC(rc, ftruncate(pidfd, 0));
    if (rc < 0)
    {
        RETRY_EINTR(close(pidfd));
        pidfd = -1;
        return -1;
    }

    l = snprintf(buf, sizeof(buf), "%d\n", getpid());
    RETRY_EINTR_RC(rc, write(pidfd, buf, l));
    if (rc != l)
    {
        syslog(LOG_ALERT, "writing pid file: %m");
        RETRY_EINTR(close(pidfd));
        pidfd = -1;
        return -1;
    }

    return 0;
}

int set_nonblocking(int sock)
{
    int rc = 0;

    // set non-blocking mode
    int flags = fcntl(sock, F_GETFL, 0);
    if (flags >= 0)
    {
        rc = fcntl(sock, F_SETFL, flags | O_NONBLOCK);
    }
    else
    {
        rc = -1;
    }

    return rc;
}

void log_uri(int level, const char *name, const char *displayname,
             osip_uri_t *uri)
{
    const char * const username = osip_uri_get_username(uri);
    const char * const host = osip_uri_get_host(uri);

    if (username || host)
    {
        if (displayname)
        {
            syslog(level, "%s %s <%s@%s>",
                   name, displayname,
                   username ? username : "", host ? host : "");
        }
        else
        {
            syslog(level, "%s <%s@%s>",
                   name, username ? username : "", host ? host : "");
        }
    }
}


int resolve_sip_addr(const char *host, int port,
                     struct sockaddr *saddr, size_t *saddr_len)
{
    uint32_t addr[4];

    if ((sock_ipv6 >= 0) && (*saddr_len >= sizeof(struct sockaddr_in6)) &&
        (inet_pton(AF_INET6, host, &addr) == 1))
    {
        *saddr_len = sizeof(struct sockaddr_in6);
        memset(saddr, 0, *saddr_len);

        ((struct sockaddr_in6 *) saddr)->sin6_family = AF_INET6;
        ((struct sockaddr_in6 *) saddr)->sin6_port = htons(port);
        memcpy(&((struct sockaddr_in6 *) saddr)->sin6_addr, addr, 16);
        return 0;
    }
    else if ((sock_ipv4 >= 0) && (*saddr_len >= sizeof(struct sockaddr_in)) &&
             (inet_pton(AF_INET, host, &addr) == 1))
    {
        *saddr_len = sizeof(struct sockaddr_in);
        memset(saddr, 0, *saddr_len);

        ((struct sockaddr_in *) saddr)->sin_family = AF_INET;
        ((struct sockaddr_in *) saddr)->sin_port = htons(port);
        memcpy(&((struct sockaddr_in *) saddr)->sin_addr, addr, 4);
        return 0;
    }

    return -1;
}

int send_message_to_addr(osip_message_t *msg, int fd,
                         const struct sockaddr *saddr, size_t saddr_len)
{
    char *data;
    size_t data_len;
    int rc;
    int sock = fd;
    struct sockaddr_in6 addr_ipv6;

    osip_message_to_str(msg, &data, &data_len);

    if ((saddr->sa_family == AF_INET) && (sock_ipv4 >= 0) &&
        ((sock < 0) || (sock == sock_ipv4)))
    {
        if (mapped_ipv4)
        {
            memset(&addr_ipv6, 0, sizeof(addr_ipv6));
            addr_ipv6.sin6_family = AF_INET6;
            addr_ipv6.sin6_port =
                ((const struct sockaddr_in *) saddr)->sin_port;
            ((uint32_t *) &addr_ipv6.sin6_addr)[0] = 0;
            ((uint32_t *) &addr_ipv6.sin6_addr)[1] = 0;
            ((uint32_t *) &addr_ipv6.sin6_addr)[2] = htonl(0xffff);
            ((uint32_t *) &addr_ipv6.sin6_addr)[3] =
                *((uint32_t *) &((const struct sockaddr_in *) saddr)->sin_addr);
            saddr = (const struct sockaddr *) &addr_ipv6;
            saddr_len = sizeof(addr_ipv6);
        }

        sock = sock_ipv4;
    }
    else if (saddr->sa_family == AF_INET6)
    {
        if (IN6_IS_ADDR_V4MAPPED(&((const struct sockaddr_in6 *) &saddr)->sin6_addr))
        {
            if (mapped_ipv4)
            {
                sock = sock_ipv4;
            }
        }
        else
        {
            sock = sock_ipv6;
        }
    }

    rc = sendto(sock, data, data_len, 0, saddr, saddr_len);
    free(data);

    return (rc > 0) ? 0: rc;
}

int compare_dns_srv(const void *l, const void *r)
{
    const struct dns_srv * const lsrv = (const struct dns_srv *) l;
    const struct dns_srv * const rsrv = (const struct dns_srv *) r;

    if (lsrv->priority != rsrv->priority)
    {
        return lsrv->priority - rsrv->priority;
    }

    return lsrv->weight - rsrv->weight;
}

int send_message_to_host(osip_message_t *msg, osip_via_t *via,
                         const char *hostname, int port)
{
    struct dns_srv dummy_srv;
    struct dns_srv *srvs;
    int i;
    int nr_srvs;
    int rc = 0;
    int diagnosed = 0;

#if defined(HAVE_UDNS)
    struct dns_rr_srv * const rr_srv =
        dns_resolve_srv(NULL, hostname, "sip", "udp", 0);
    if (rr_srv != NULL)
    {
        srvs = rr_srv->dnssrv_srv;
        nr_srvs = rr_srv->dnssrv_nrr;
        qsort(srvs, nr_srvs, sizeof(struct dns_srv), compare_dns_srv);
    }
    else
#endif
    {
        dummy_srv.priority = 0;
        dummy_srv.weight = 0;
        dummy_srv.port = port;
        dummy_srv.name = (char *) hostname;
        srvs = &dummy_srv;
        nr_srvs = 1;
    }

    i = 0;
    while (i < nr_srvs)
    {
        struct addrinfo hints;
        struct addrinfo *ainfo;
        const int priority = srvs[i].priority;
        int j, end;
        int tweight = 0;
        int rweight;

        for (end = i;
             (end < nr_srvs) && (srvs[end].priority == priority);
             end++)
        {
            if (srvs[end].weight)
            {
                tweight += srvs[end].weight;
            }
            else
            {
                tweight++;
            }
        }

        {
            unsigned long long r = rand();
            r *= tweight;
            rweight = r / (RAND_MAX + 1LL);
        }

        for (j = i; j < end; j++)
        {
            const int weight = srvs[j].weight;

            rweight -= weight;
            if (!weight)
            {
                rweight--;
            }

            if (rweight < 0)
            {
                break;
            }
        }

        if (j >= end)
        {
            j = end - 1;
        }

        memset(&hints, 0, sizeof(hints));

        if (sock_ipv6 < 0)
        {
            hints.ai_family = AF_INET;
        }
        else if (sock_ipv4 < 0)
        {
            hints.ai_family = AF_INET6;
        }
        else if (mapped_ipv4)
        {
            hints.ai_family = AF_INET6;
#if defined(AI_V4MAPPED)
            /* NetBSD apparently doesn't define AI_V4MAPPED */
            hints.ai_flags = AI_V4MAPPED;
#endif
        }
        else
        {
            hints.ai_family = AF_UNSPEC;
        }

        hints.ai_socktype = SOCK_DGRAM;

        if (!(rc = getaddrinfo(srvs[j].name, "sip", &hints, &ainfo)))
        {
            const int port = srvs[j].port;
            struct addrinfo *curr_ainfo = ainfo;

            rc = -1;
            while (curr_ainfo != NULL)
            {
                if (port != 0)
                {
                    if (curr_ainfo->ai_family == AF_INET)
                    {
                        ((struct sockaddr_in *) curr_ainfo->ai_addr)->sin_port =
                            htons(port);
                    }
                    else if (curr_ainfo->ai_family == AF_INET6)
                    {
                        ((struct sockaddr_in6 *) curr_ainfo->ai_addr)->sin6_port =
                            htons(port);
                    }
                }

                if ((curr_ainfo->ai_family == AF_INET) ||
                    ((curr_ainfo->ai_family == AF_INET6) &&
                     (IN6_IS_ADDR_V4MAPPED(&((struct sockaddr_in6 *) curr_ainfo->ai_addr)->sin6_addr))))
                {
                    osip_via_set_host(via, addrstr_ipv4);
                }
                else if (curr_ainfo->ai_family == AF_INET6)
                {
                    osip_via_set_host(via, addrstr_ipv6);
                }

                if (!(rc = send_message_to_addr(msg, -1, curr_ainfo->ai_addr,
                                                curr_ainfo->ai_addrlen)))
                {
                    osip_via_set_host(via, NULL);
                    break;
                }

                osip_via_set_host(via, NULL);
                curr_ainfo = curr_ainfo->ai_next;
            }

            if (rc != 0)
            {
                if (end >= nr_srvs)
                {
                    syslog(LOG_ERR, "unable to send message to %s: %m",
                           hostname);
                    diagnosed = 1;
                }
                else
                {
                    syslog(LOG_WARNING, "unable to send message to %s: %m",
                           hostname);
                }
            }

            freeaddrinfo(ainfo);

            if (rc == 0)
            {
                break;
            }
        }

        i = end;
    }

#if defined(HAVE_UDNS)
    free(rr_srv);
#endif

    if ((i >= nr_srvs) && !diagnosed)
    {
        syslog(LOG_ERR, "unable to resolve hostname %s (error %d)",
               hostname, rc);
    }

    return (i >= nr_srvs) ? -1 : 0;
}

void send_response_message(osip_message_t *msg, osip_via_t *via, int fd)
{
    struct sockaddr_storage saddr;
    size_t saddr_len = sizeof(saddr);
    osip_generic_param_t *received = NULL;
    osip_generic_param_t *rport = NULL;
    const char *host = NULL;
    int port = via->port ? atoi(via->port) : 5060;

    if (verbose || (msg->status_code >= 200))
    {
        syslog((msg->status_code >= 200) ? LOG_INFO : LOG_DEBUG,
               "response %d", msg->status_code);
    }

    if (verbose)
    {
        osip_from_t * const from = osip_message_get_from(msg);
        osip_from_t * const to = osip_message_get_to(msg);

        log_uri(LOG_DEBUG, "-From",
                osip_from_get_displayname(from),
                osip_from_get_url(from));
        log_uri(LOG_DEBUG, "-To",
                osip_to_get_displayname(to),
                osip_to_get_url(to));
    }

    osip_via_param_get_byname(via, "received", &received);
    if (received)
    {
        host = received->gvalue;
    }
    else
    {
        host = via->host;
    }

    osip_via_param_get_byname(via, "rport", &rport);
    if (rport)
    {
        port = osip_atoi(rport->gvalue);
    }

    if (!resolve_sip_addr(host, port,
                          (struct sockaddr *) &saddr, &saddr_len))
    {
        if (verbose || (msg->status_code >= 200))
        {
            syslog((msg->status_code >= 200) ? LOG_INFO : LOG_DEBUG,
                   "-forwarding to %s:%d",
                   host, port);
        }

        send_message_to_addr(msg, fd, (struct sockaddr *) &saddr, saddr_len);
    }
}

void process_message(int conn_nr, const struct sockaddr *peer_addr,
                     socklen_t peer_addrlen, osip_message_t *msg)
{
    if (MSG_IS_REQUEST(msg))
    {
        osip_uri_t *remote_uri = NULL;
        osip_via_t *via = (osip_via_t *) osip_list_get(&msg->vias, 0);
        int res;
        char buf[1024];

        if (!msg->sip_method)
        {
            return;
        }

        {
            osip_generic_param_t *rport;
            struct sockaddr_storage addr_storage;
            size_t addr_storage_len = sizeof(addr_storage);
            struct sockaddr_in6 * const addr_ipv6 =
                (struct sockaddr_in6 *) &addr_storage;
            struct sockaddr_in * const addr_ipv4 =
                (struct sockaddr_in *) &addr_storage;
            const struct sockaddr_in6 * const peer_ipv6 =
                (const struct sockaddr_in6 *) peer_addr;
            const struct sockaddr_in * const peer_ipv4 =
                (const struct sockaddr_in *) peer_addr;
            int different = 1;

            if (!via)
            {
                return;
            }

            osip_via_param_get_byname(via, "rport", &rport);

            if (!rport &&
                !resolve_sip_addr(via->host, 5060,
                                  (struct sockaddr *) &addr_storage,
                                  &addr_storage_len))
            {
                if ((peer_addr->sa_family == AF_INET6) &&
                    (addr_storage.ss_family == AF_INET))
                {
                    different = !IN6_IS_ADDR_V4MAPPED(&peer_ipv6->sin6_addr) ||
                        memcmp(((in_addr_t *) &peer_ipv6->sin6_addr) + 3,
                               &addr_ipv4->sin_addr, 4);
                }
                else if ((peer_addr->sa_family == AF_INET6) &&
                         (addr_storage.ss_family == AF_INET6))
                {
                    different = memcmp(&peer_ipv6->sin6_addr,
                                       &addr_ipv6->sin6_addr, 16);
                }
                else if ((peer_addr->sa_family == AF_INET) &&
                         (addr_storage.ss_family == AF_INET))
                {
                    different = memcmp(&peer_ipv4->sin_addr,
                                       &addr_ipv4->sin_addr, 4);
                }
            }

            if (different)
            {
                char addr_str[INET6_ADDRSTRLEN];
                addr_str[0] = '\0';

                getnameinfo(peer_addr, peer_addrlen, addr_str,
                            sizeof(addr_str), NULL, 0, NI_NUMERICHOST);
                if (addr_str[0])
                {
                    osip_via_set_received(via, osip_strdup(addr_str));
                }
            }

            if (rport)
            {
                const int port = (peer_addr->sa_family == AF_INET6) ?
                    ntohs(peer_ipv6->sin6_port) :
                    (peer_addr->sa_family == AF_INET) ?
                    ntohs(peer_ipv4->sin_port) : 0;

                if (port)
                {
                    char buf[16];
                    sprintf(buf, "%d", port);
                    rport->gvalue = osip_strdup(buf);
                }
            }
        }

        {
            const osip_uri_t * const uri = osip_message_get_uri(msg);

            if (uri->host && uri->username)
            {
                int domain_id = -1;
                osip_from_t * const from = osip_message_get_from(msg);
                osip_from_t * const to = osip_message_get_to(msg);

                syslog(LOG_INFO, "%s %s@%s", msg->sip_method, uri->username,
                       uri->host);

                log_uri(LOG_INFO, "-To", osip_to_get_displayname(to),
                        osip_to_get_url(to));
                log_uri(LOG_INFO, "-From", osip_from_get_displayname(from),
                        osip_from_get_url(from));


                sqlite3_bind_text(stmt_domain, 1, uri->host, strlen(uri->host),
                                  SQLITE_STATIC);
                if ((res = sqlite3_step(stmt_domain)) == SQLITE_ROW)
                {
                    domain_id = sqlite3_column_int(stmt_domain, 0);
                    sqlite3_bind_text(stmt_user, 1, uri->username,
                                      strlen(uri->username), SQLITE_STATIC);
                    sqlite3_bind_int(stmt_user, 2, domain_id);

                    if ((res = sqlite3_step(stmt_user)) == SQLITE_ROW)
                    {
                        const char *remote_str =
                            (const char *) sqlite3_column_text(stmt_user, 0);

                        if (remote_str == NULL)
                        {
                            syslog(LOG_WARNING, "%s@%s not known",
                                   uri->username, uri->host);
                        }
                        else
                        {
                            osip_uri_init(&remote_uri);
                            if ((osip_uri_parse(remote_uri, remote_str) < 0) ||
                                !remote_uri->username || !remote_uri->host)
                            {
                                syslog(LOG_WARNING, "unable to parse forwarding uri %s",
                                       remote_str);
                                osip_uri_free(remote_uri);
                                remote_uri = NULL;
                            }
                        }

                        res = sqlite3_step(stmt_user);
                        if (res != SQLITE_DONE)
                        {
                            syslog(LOG_ERR, "SQLite error %s", sqlite3_errmsg(db));
                        }
                    }

                    sqlite3_reset(stmt_user);

                    res = sqlite3_step(stmt_domain);
                    if (res != SQLITE_DONE)
                    {
                        syslog(LOG_ERR, "SQLite error %s", sqlite3_errmsg(db));
                    }
                }

                sqlite3_reset(stmt_domain);

                if (domain_id <= 0)
                {
                    // ignore request if we are not even handling the domain
                    return;
                }
            }
            else
            {
                // no point if we didn't receive a useful request
                return;
            }
        }

        if (remote_uri == NULL)
        {
            int i;
            const int fd = (conn_nr >= 0) ? sock[conn_nr] : -1;
            osip_message_t *rmsg = NULL;

            if (!strcmp(msg->sip_method, "ACK") ||
                !strcmp(msg->sip_method, "REGISTER"))
            {
                // if it was an ACK/REGISTER request, there is no point
                // sending back a 404 response, just ignore it
                return;
            }

            osip_message_init(&rmsg);

            osip_message_set_version(rmsg, osip_strdup(osip_message_get_version(msg)));
            osip_message_set_status_code(rmsg, 404);
            osip_message_set_reason_phrase(rmsg, osip_strdup("User Not Known"));
            osip_from_clone(msg->from, &rmsg->from);
            osip_from_clone(msg->to, &rmsg->to);
            osip_cseq_clone(msg->cseq, &rmsg->cseq);
            osip_call_id_clone(msg->call_id, &rmsg->call_id);

            i = 0;
            while (!osip_list_eol(&msg->vias, i))
            {
                osip_via_t *via;

                osip_via_clone(osip_list_get(&msg->vias, i), &via);
                osip_list_add(&rmsg->vias, via, -1);
                i++;
            }

            send_response_message(rmsg, via, fd);
            osip_message_free(rmsg);

            return;
        }

        syslog(LOG_INFO, "-forwarding to %s:%s@%s",
               remote_uri->scheme, remote_uri->username, remote_uri->host);


        osip_uri_free(osip_message_get_uri(msg));
        osip_message_set_uri(msg, remote_uri);

        // updated max-forwards
        {
            osip_header_t *max_forwards;
            osip_message_get_max_forwards(msg, 0, &max_forwards);
            if (max_forwards)
            {
                int forwards_count = 70;

                if (max_forwards->hvalue)
                {
                    forwards_count = atoi(max_forwards->hvalue) - 1;
                }

                snprintf(buf, sizeof(buf), "%d", forwards_count);
                osip_message_set_max_forwards(msg, buf);
            }
            else
            {
                osip_message_set_max_forwards(msg, "70");
            }
        }

        // add Via entry
        {
            osip_generic_param_t *branch = NULL;
            osip_via_t *old_via = NULL;

            old_via = (osip_via_t *) osip_list_get(&msg->vias, 0);
            osip_via_param_get_byname(old_via, "branch", &branch);

            via = NULL;
            osip_via_init(&via);
            osip_via_set_version(via, osip_strdup("2.0"));
            osip_via_set_protocol(via, osip_strdup("UDP"));
            osip_via_set_port(via, osip_strdup(local_port_str));

            if (branch && branch->gvalue)
            {
                // add incoming socket number to branch in outgoing message
                const size_t l = strlen(branch->gvalue);

                if ((l + 2 < sizeof(buf)) && (conn_nr < nr_sock))
                {
                    memcpy(buf, branch->gvalue, l);
                    buf[l] = '.';
                    buf[l + 1] = 'A' + conn_nr;
                    buf[l + 2] = '\0';
                    osip_via_set_branch(via, osip_strdup(buf));
                }
                else
                {
                    osip_via_set_branch(via, osip_strdup(branch->gvalue));
                }
            }

            osip_list_add(&msg->vias, via, 0);
        }

        if (send_message_to_host(msg, via, remote_uri->host,
                                 remote_uri->port ? atoi(remote_uri->port) : 0) < 0)
        {
            // TODO: send error message
        }
    }
    else if (MSG_IS_RESPONSE(msg))
    {
        int discard = 1;
        int fd = -1;

        {
            // check Via header
            osip_via_t * const via =
                (osip_via_t *) osip_list_get(&msg->vias, 0);

            const char *via_host = osip_via_get_host(via);
            if (via_host)
            {
                discard =
                    !(*addrstr_ipv4 && !strcmp(via_host, addrstr_ipv4)) &&
                    !(*addrstr_ipv6 && !strcmp(via_host, addrstr_ipv6));
            }

            if (!discard)
            {
                osip_generic_param_t *branch = NULL;
                osip_via_param_get_byname(via, "branch", &branch);

                if (branch && branch->gvalue)
                {
                    // get socket number from branch parameter
                    const size_t l = strlen(branch->gvalue);
                    if ((l > 2) && (branch->gvalue[l - 2] == '.'))
                    {
                        const int nr = branch->gvalue[l - 1] - 'A';
                        if ((nr >= 0) && (nr < nr_sock))
                        {
                            fd = sock[nr];
                        }
                    }
                }
            }

            osip_via_free(via);
            osip_list_remove(&msg->vias, 0);
        }

        if (!discard)
        {
            osip_via_t * const via =
                (osip_via_t *) osip_list_get(&msg->vias, 0);

            if (via != NULL)
            {
                send_response_message(msg, via, fd);
            }
            else
            {
                syslog(LOG_WARNING, "Via-header missing in %d reponse",
                       msg->status_code);
            }
        }
    }
}

int main(int argc, char *argv[])
{
    struct sigaction act;
    const char *pidname = "/run/sipfwd/sipfwd.pid";
    const char *dbname = "/var/lib/sipfwd/sipfwd.db";
    const char *tail;
    const char db_domain_query[] = "SELECT id FROM domain WHERE name=LOWER(?)";
    const char db_user_query[] = "SELECT remote FROM forward WHERE user=? AND domain_id=?";
    const char *progname = basename(argv[0]);
    int force_ipv4 = 0;
    int force_ipv6 = 0;
    int daemonpipe = -1;
    int c;
    int foreground = 0;

    while ((c = getopt(argc, argv, "46b:d:fhp:v")) != -1)
    {
        switch (c)
        {
        case '4':
            force_ipv4 = 1;
            force_ipv6 = 0;
            break;

        case '6':
            force_ipv6 = 1;
            force_ipv4 = 0;
            break;

        case 'b':
        {
            if (nr_sock >= sizeof(sock) / sizeof(*sock))
            {
                break;
            }

            if (!force_ipv4)
            {
                struct sockaddr_in6 addr;

                memset(&addr, 0, sizeof(addr));
                if (inet_pton(AF_INET6, optarg, &addr.sin6_addr) == 1)
                {
                    sock[nr_sock] = socket(PF_INET6, SOCK_DGRAM, 0);

                    addr.sin6_family = AF_INET6;
                    addr.sin6_port = htons(local_port);

                    if (bind(sock[nr_sock],
                             (struct sockaddr *) &addr, sizeof(addr)) < 0)
                    {
                        perror("bind");
                        RETRY_EINTR(close(sock[nr_sock]));
                        quit = 1;
                    }
                    else
                    {
                        if ((sock_ipv6 < 0) &&
                            !IN6_IS_ADDR_V4MAPPED(&addr.sin6_addr))
                        {
                            sock_ipv6 = sock[nr_sock];

                            if (!IN6_IS_ADDR_UNSPECIFIED(&addr.sin6_addr))
                            {
                                inet_ntop(AF_INET6, &addr.sin6_addr,
                                          addrstr_ipv6,
                                          sizeof(addrstr_ipv6));
                            }
                        }
                        if (!force_ipv6 && (sock_ipv4 < 0) &&
                            (IN6_IS_ADDR_UNSPECIFIED(&addr.sin6_addr) ||
                             IN6_IS_ADDR_V4MAPPED(&addr.sin6_addr)))
                        {
                            sock_ipv4 = sock[nr_sock];
                            mapped_ipv4 = 1;

                            if (IN6_IS_ADDR_V4MAPPED(&addr.sin6_addr))
                            {
                                in_addr_t * const addr_ipv4 =
                                    ((in_addr_t *) &addr.sin6_addr) + 3;
                                if (*addr_ipv4 != INADDR_ANY)
                                {
                                    inet_ntop(AF_INET, addr_ipv4,
                                              addrstr_ipv4,
                                              sizeof(addrstr_ipv4));
                                }
                            }
                        }
                        nr_sock++;
                    }
                    break;
                }
            }

            if (!force_ipv6)
            {
                struct sockaddr_in addr;

                memset(&addr, 0, sizeof(addr));
                if (inet_pton(AF_INET, optarg, &addr.sin_addr) == 1)
                {
                    sock[nr_sock] = socket(PF_INET, SOCK_DGRAM, 0);

                    addr.sin_family = AF_INET;
                    addr.sin_port = htons(local_port);

                    if (bind(sock[nr_sock],
                             (struct sockaddr *) &addr, sizeof(addr)) < 0)
                    {
                        perror("bind");
                        RETRY_EINTR(close(sock[nr_sock]));
                        quit = 1;
                    }
                    else
                    {
                        if ((sock_ipv4 < 0) || mapped_ipv4)
                        {
                            sock_ipv4 = sock[nr_sock];
                            mapped_ipv4 = 0;

                            if (addr.sin_addr.s_addr != INADDR_ANY)
                            {
                                inet_ntop(AF_INET, &addr.sin_addr,
                                          addrstr_ipv4,
                                          sizeof(addrstr_ipv4));
                            }
                        }
                        nr_sock++;
                    }
                    break;
                }
            }

            fprintf(stderr, "%s: unable to bind to %s\n", progname, optarg);
            quit = 1;
        }
        break;

        case 'd':
            dbname = optarg;
            break;

        case 'f':
            foreground = 1;
            break;

        case 'h':
            fprintf(stderr, "%s [-4] [-6] [-b bindaddr] [-f] [-d dbname] [-p pidfile] [-v]\n", progname);
            quit = 1;
            break;

        case 'p':
            pidname = optarg;
            break;

        case 'v':
            verbose = 1;
            break;

        case '?':
            quit = 1;
            break;
        }
    }

    if (!quit && (nr_sock == 0))
    {
        if (!force_ipv4)
        {
            struct sockaddr_in6 addr;
            memset(&addr, 0, sizeof(addr));

            sock[nr_sock] = socket(PF_INET6, SOCK_DGRAM, 0);

            addr.sin6_family = AF_INET6;
            addr.sin6_port = htons(local_port);
            addr.sin6_addr = in6addr_any;

            if (bind(sock[nr_sock],
                     (struct sockaddr *) &addr, sizeof(addr)) < 0)
            {
                if ((errno != EADDRNOTAVAIL) && (errno != EAFNOSUPPORT))
                {
                    perror("bind");
                    quit = 1;
                }

                RETRY_EINTR(close(sock[nr_sock]));
            }
            else
            {
                if (sock_ipv6 < 0)
                {
                    sock_ipv6 = sock[nr_sock];
                }
                if (!force_ipv6 && (sock_ipv4 < 0))
                {
                    sock_ipv4 = sock[nr_sock];
                    mapped_ipv4 = 1;
                }
                nr_sock++;
            }
        }

        if (!force_ipv6 && !quit && (nr_sock == 0))
        {
            struct sockaddr_in addr;
            memset(&addr, 0, sizeof(addr));

            sock[nr_sock] = socket(PF_INET, SOCK_DGRAM, 0);

            addr.sin_family = AF_INET;
            addr.sin_port = htons(local_port);
            addr.sin_addr.s_addr = INADDR_ANY;

            if (bind(sock[nr_sock],
                     (struct sockaddr *) &addr, sizeof(addr)) < 0)
            {
                perror("bind");
                RETRY_EINTR(close(sock[nr_sock]));
                quit = 1;
            }
            else
            {
                if ((sock_ipv4 < 0) || mapped_ipv4)
                {
                    sock_ipv4 = sock[nr_sock];
                    mapped_ipv4 = 0;
                }
                nr_sock++;
            }
        }
    }

    if (((sock_ipv4 >= 0) && !*addrstr_ipv4) ||
        ((sock_ipv6 >= 0) && !*addrstr_ipv6))
    {
        char hostname[256];

        if (!gethostname(hostname, sizeof(hostname)))
        {
            struct addrinfo hints;
            struct addrinfo *ainfo;

            memset(&hints, 0, sizeof(hints));

            hints.ai_flags = AI_PASSIVE | AI_CANONNAME;
            hints.ai_family = AF_UNSPEC;
            hints.ai_protocol = IPPROTO_UDP;
            hints.ai_socktype = SOCK_DGRAM;

            if (!getaddrinfo(hostname, "", &hints, &ainfo))
            {
                struct addrinfo *curr_ainfo = ainfo;
                const char *canonname = NULL;

                while (curr_ainfo != NULL)
                {
                    if (curr_ainfo->ai_canonname && !canonname)
                    {
                        canonname = curr_ainfo->ai_canonname;
                    }

                    if ((curr_ainfo->ai_family == AF_INET) &&
                        !*addrstr_ipv4 && (sock_ipv4 >= 0))
                    {
                        const struct in_addr *addr_ipv4 =
                            &((struct sockaddr_in *) curr_ainfo->ai_addr)->sin_addr;
                        if (((uint8_t *) addr_ipv4)[0] != 127)
                        {
                            inet_ntop(AF_INET, addr_ipv4,
                                      addrstr_ipv4, sizeof(addrstr_ipv4));
                        }
                    }
                    else if ((curr_ainfo->ai_family == AF_INET6) &&
                             !*addrstr_ipv6 && (sock_ipv6 >= 0))
                    {
                        const struct in6_addr *addr_ipv6 =
                            &((struct sockaddr_in6 *) curr_ainfo->ai_addr)->sin6_addr;
                        if (!IN6_IS_ADDR_LOOPBACK(addr_ipv6))
                        {
                            inet_ntop(AF_INET6, addr_ipv6,
                                      addrstr_ipv6, sizeof(addrstr_ipv6));
                        }
                    }

                    curr_ainfo = curr_ainfo->ai_next;
                }

                if (canonname && (sock_ipv4 >= 0) && !*addrstr_ipv4)
                {
                    strcpy(addrstr_ipv4, canonname);
                }

                if (canonname && (sock_ipv6 >= 0) && !*addrstr_ipv6)
                {
                    strcpy(addrstr_ipv6, canonname);
                }

                freeaddrinfo(ainfo);
            }
        }
    }

    if (quit)
    {
        return 1;
    }

    openlog(progname, LOG_NDELAY, LOG_MAIL);

#if defined(HAVE_UDNS)
    if (dns_init(NULL, 1) < 0)
    {
        syslog(LOG_ERR, "failed to initialise DNS resolver");
        closelog();
        return 1;
    }
#endif

    srand(time(NULL));

    if ((sock_ipv4 >= 0) && *addrstr_ipv4)
    {
        syslog(LOG_INFO, "canonical IPv4 address/host: %s", addrstr_ipv4);
    }

    if ((sock_ipv6 >= 0) && *addrstr_ipv6)
    {
        syslog(LOG_INFO, "canonical IPv6 address/host: %s", addrstr_ipv6);
    }

    act.sa_handler = &signal_quit;
    sigemptyset(&act.sa_mask);
    act.sa_flags = 0;
    if (sigaction(SIGINT, &act, NULL) < 0)
    {
        syslog(LOG_ERR, "sigaction: %m");
    }

    act.sa_handler = &signal_quit;
    sigemptyset(&act.sa_mask);
    act.sa_flags = 0;
    if (sigaction(SIGTERM, &act, NULL) < 0)
    {
        syslog(LOG_ERR, "sigaction: %m");
    }

    if (!foreground)
    {
        if ((daemonpipe = daemonize()) < 0)
        {
            return 1;
        }
    }

    if (daemon_init(pidname))
    {
        if (daemonpipe >= 0) RETRY_EINTR(close(daemonpipe));
        return 1;
    }


    {
        int i;
        for (i = 0; i < nr_sock; i++)
        {
            if (set_nonblocking(sock[i]) == -1)
            {
                syslog(LOG_ALERT, "fcntl: %m");
                closelog();
                if (daemonpipe >= 0) RETRY_EINTR(close(daemonpipe));
                return 1;
            }
        }
    }


    if (SQLITE_OK != sqlite3_open(dbname, &db))
    {
        syslog(LOG_ALERT, "Error opening SQLite database (%s)",
               sqlite3_errmsg(db));
        sqlite3_close(db);
        if (daemonpipe >= 0) RETRY_EINTR(close(daemonpipe));
        return 1;
    }

    sqlite3_busy_timeout(db, 10000);

    if (SQLITE_OK != sqlite3_prepare(db, db_domain_query,
                                     sizeof(db_domain_query),
                                     &stmt_domain, &tail))
    {
        syslog(LOG_ALERT, "Error preparing SQLite query (%s)",
               sqlite3_errmsg(db));
        sqlite3_close(db);
        if (daemonpipe >= 0) RETRY_EINTR(close(daemonpipe));
        return 1;
    }

    if (SQLITE_OK != sqlite3_prepare(db, db_user_query, sizeof(db_user_query),
                                     &stmt_user, &tail))
    {
        syslog(LOG_ALERT, "Error preparing SQLite query (%s)",
               sqlite3_errmsg(db));
        sqlite3_close(db);
        if (daemonpipe >= 0) RETRY_EINTR(close(daemonpipe));
        return 1;
    }

    parser_init();

    if (daemonpipe >= 0)
    {
        int rc;

        RETRY_EINTR_RC(rc, write(daemonpipe, "\0", 1));
        if (rc < 0)
        {
            syslog(LOG_ALERT, "write daemon pipe: %m");
            sqlite3_close(db);
            closelog();
            RETRY_EINTR(close(pidfd));
            RETRY_EINTR(close(daemonpipe));
            return 1;
        }

        RETRY_EINTR(close(daemonpipe));
        daemonpipe = -1;
    }

    while (!quit)
    {
        struct pollfd pfd[sizeof(sock) / sizeof(sock[0])];
        unsigned int i;
        int rc;

        for (i = 0; i < nr_sock; i++)
        {
            pfd[i].fd = sock[i];
            pfd[i].events = POLLIN;
        }

        RETRY_EINTR_RC(rc, poll(pfd, nr_sock, -1));
        if (rc < 0)
        {
            if (!quit)
            {
                syslog(LOG_ERR, "poll: %m");
            }
            break;
        }
        else if (rc == 0)
        {
            // poll timed out
            continue;
        }

        for (i = 0; i < nr_sock; i++)
        {
            const int fd = pfd[i].fd;

            if (pfd[i].revents & POLLIN)
            {
                char buf[64*1024];
                struct sockaddr_in6 peer_addr;
                socklen_t addrlen = sizeof(peer_addr);

                RETRY_EINTR_RC(rc, recvfrom(fd, buf, sizeof(buf), 0,
                                            (struct sockaddr *) &peer_addr,
                                            &addrlen));
                if (rc > 0)
                {
                    osip_message_t *msg = NULL;

                    osip_message_init(&msg);
                    osip_message_parse(msg, buf, rc);

                    process_message(i, (struct sockaddr *) &peer_addr, addrlen,
                                    msg);

                    osip_message_free(msg);
                }
                else
                {
                    syslog(LOG_WARNING,
                           "recvfrom socket %d returned error: %m", fd);
                }
            }
        }
    }

    sqlite3_close(db);
    closelog();
    RETRY_EINTR(close(pidfd));

    return 0;
}
