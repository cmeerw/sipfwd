#! /bin/sh
### BEGIN INIT INFO
# Provides:          sipfwd
# Required-Start:    $remote_fs $syslog $named
# Required-Stop:     $remote_fs $syslog $named
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Description:       SIP forwarding daemon
### END INIT INFO

set -e

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
DESC="SIP forwarding daemon"
NAME=sipfwd
DAEMON=/usr/sbin/$NAME
PIDFILE=/var/run/sipfwd/$NAME.pid
SCRIPTNAME=/etc/init.d/$NAME

# Gracefully exit if the package has been removed.
test -x $DAEMON || exit 0

#
#	Function that starts the daemon/service.
#
d_start() {
	if [ ! -d /var/run/sipfwd ]; then
	  mkdir -m 0750 /var/run/sipfwd
	  chown sipfwd.root /var/run/sipfwd
	fi

	start-stop-daemon --start --quiet --pidfile $PIDFILE \
		--chuid sipfwd:daemon --exec $DAEMON
}

#
#	Function that stops the daemon/service.
#
d_stop() {
	start-stop-daemon --stop --quiet --pidfile $PIDFILE \
		--name $NAME
}

#
#	Function that sends a SIGHUP to the daemon/service.
#
d_reload() {
	start-stop-daemon --stop --quiet --pidfile $PIDFILE \
		--name $NAME --signal 1
}

case "$1" in
  start)
	echo -n "Starting $DESC: $NAME"
	d_start
	echo "."
	;;
  stop)
	echo -n "Stopping $DESC: $NAME"
	d_stop
	echo "."
	;;
  restart|force-reload)
	echo -n "Restarting $DESC: $NAME"
	d_stop
	sleep 1
	d_start
	echo "."
	;;
  *)
	echo "Usage: $SCRIPTNAME {start|stop|restart|force-reload}" >&2
	exit 1
	;;
esac

exit 0
