CFLAGS += -O2 -Wall -DHAVE_UDNS

all:	sipfwd

install:	sipfwd
	install -d $(DESTDIR)/usr/sbin \
		$(DESTDIR)/usr/share/doc/sipfwd \
		$(DESTDIR)/usr/share/man/man8 \

	install -s sipfwd $(DESTDIR)/usr/sbin/
	install -m 0644 db.sqlite $(DESTDIR)/usr/share/doc/sipfwd/

clean:
	rm -f *.o

veryclean:  clean
	rm -f sipfwd


sipfwd:	sipfwd.o
	$(CC) $(CFLAGS) -o $@ $^ -lsqlite3 -losipparser2 -ludns -lpthread
